Variable Entity : Type.
Variable float : Type.
Variable part_of : Entity -> Entity -> Prop.
Variable _具体物: Entity -> Prop.
Variable _列車: Entity -> Prop.
Variable _機関車: Entity -> Prop.
Variable _内燃機関車: Entity -> Prop.
Variable _ラッセル車: Entity -> Prop.
Variable _電気機関車: Entity -> Prop.
Variable _人間: Entity -> Prop.
Variable _ディxmdashxゼル機関車: Entity -> Prop.
Variable _DD15形: Entity -> Prop.
Variable _蒸気機関車: Entity -> Prop.
Variable _客車: Entity -> Prop.
Variable _先頭車: Entity -> Prop.
Variable _先頭制御車: Entity -> Prop.
Variable _先頭付随車: Entity -> Prop.
Variable _中間車: Entity -> Prop.
Variable _24系: Entity -> Prop.
Variable _部品: Entity -> Prop.
Variable _運転台: Entity -> Prop.
Variable _台車: Entity -> Prop.
Variable _動力: Entity -> Prop.
Variable _ディxmdashxゼルエンジン: Entity -> Prop.
Variable _モxmdashxタxmdashx: Entity -> Prop.
Variable _除雪板: Entity -> Prop.
Variable _EF81形: Entity -> Prop.
Variable _抽象物: Entity -> Prop.
Variable _列車名: Entity -> Prop.
Variable _北斗星: Entity -> Prop.
Variable _25系: Entity -> Prop.
Variable _サxmdashxビス: Entity -> Prop.
Variable _食事提供: Entity -> Prop.
Variable _作業: Entity -> Prop.
Variable _除雪作業: Entity -> Prop.
Variable _運行サxmdashxビス: Entity -> Prop.
Variable _電車: Entity -> Prop.
Variable _気動車: Entity -> Prop.
Variable _車輪: Entity -> Prop.
Variable _運転士: Entity -> Prop.
Variable _重さ: Entity -> float.
Variable _長さ: Entity -> float.
Variable _除雪装置: Entity -> Prop.
Variable _作業内容: Entity -> Prop.
Variable _サxmdashxビス内容: Entity -> Prop.
Variable _牽引車: Entity -> Prop.
Variable _1sim5号車lpar禁煙開放B寝台rpar: Entity -> Prop.
Variable _6号車lpar喫煙B寝台plusロビxmdashxrpar: Entity -> Prop.
Variable _7号車lpar食堂車rpar: Entity -> Prop.
Variable _8号車lpar喫煙A寝台rpar: Entity -> Prop.
Variable _9middot10号車lpar喫煙A寝台plusB寝台rpar: Entity -> Prop.
Variable _11号車lpar喫煙開放B寝台rpar: Entity -> Prop.
Variable _乗客: Entity -> Prop.
Variable _車掌: Entity -> Prop.
Axiom ax_isa_列車_具体物 : forall x: Entity, _列車(x) -> _具体物(x).
Hint Resolve ax_isa_列車_具体物.
Axiom ax_isa_機関車_列車 : forall x: Entity, _機関車(x) -> _列車(x).
Hint Resolve ax_isa_機関車_列車.
Axiom ax_isa_内燃機関車_機関車 : forall x: Entity, _内燃機関車(x) -> _機関車(x).
Hint Resolve ax_isa_内燃機関車_機関車.
Axiom ax_isa_ラッセル車_DD15形 : forall x: Entity, _ラッセル車(x) -> _DD15形(x).
Hint Resolve ax_isa_ラッセル車_DD15形.
Axiom ax_isa_電気機関車_機関車 : forall x: Entity, _電気機関車(x) -> _機関車(x).
Hint Resolve ax_isa_電気機関車_機関車.
Axiom ax_isa_ディxmdashxゼル機関車_内燃機関車 : forall x: Entity, _ディxmdashxゼル機関車(x) -> _内燃機関車(x).
Hint Resolve ax_isa_ディxmdashxゼル機関車_内燃機関車.
Axiom ax_isa_DD15形_ディxmdashxゼル機関車 : forall x: Entity, _DD15形(x) -> _ディxmdashxゼル機関車(x).
Hint Resolve ax_isa_DD15形_ディxmdashxゼル機関車.
Axiom ax_isa_蒸気機関車_機関車 : forall x: Entity, _蒸気機関車(x) -> _機関車(x).
Hint Resolve ax_isa_蒸気機関車_機関車.
Axiom ax_isa_客車_列車 : forall x: Entity, _客車(x) -> _列車(x).
Hint Resolve ax_isa_客車_列車.
Axiom ax_isa_先頭車_電車 : forall x: Entity, _先頭車(x) -> _電車(x).
Hint Resolve ax_isa_先頭車_電車.
Axiom ax_isa_先頭制御車_先頭車 : forall x: Entity, _先頭制御車(x) -> _先頭車(x).
Hint Resolve ax_isa_先頭制御車_先頭車.
Axiom ax_isa_先頭付随車_先頭車 : forall x: Entity, _先頭付随車(x) -> _先頭車(x).
Hint Resolve ax_isa_先頭付随車_先頭車.
Axiom ax_isa_中間車_電車 : forall x: Entity, _中間車(x) -> _電車(x).
Hint Resolve ax_isa_中間車_電車.
Axiom ax_isa_24系_客車 : forall x: Entity, _24系(x) -> _客車(x).
Hint Resolve ax_isa_24系_客車.
Axiom ax_isa_部品_具体物 : forall x: Entity, _部品(x) -> _具体物(x).
Hint Resolve ax_isa_部品_具体物.
Axiom ax_isa_運転台_部品 : forall x: Entity, _運転台(x) -> _部品(x).
Hint Resolve ax_isa_運転台_部品.
Axiom ax_isa_台車_部品 : forall x: Entity, _台車(x) -> _部品(x).
Hint Resolve ax_isa_台車_部品.
Axiom ax_isa_動力_部品 : forall x: Entity, _動力(x) -> _部品(x).
Hint Resolve ax_isa_動力_部品.
Axiom ax_isa_ディxmdashxゼルエンジン_動力 : forall x: Entity, _ディxmdashxゼルエンジン(x) -> _動力(x).
Hint Resolve ax_isa_ディxmdashxゼルエンジン_動力.
Axiom ax_isa_モxmdashxタxmdashx_動力 : forall x: Entity, _モxmdashxタxmdashx(x) -> _動力(x).
Hint Resolve ax_isa_モxmdashxタxmdashx_動力.
Axiom ax_isa_除雪板_部品 : forall x: Entity, _除雪板(x) -> _部品(x).
Hint Resolve ax_isa_除雪板_部品.
Axiom ax_isa_EF81形_電気機関車 : forall x: Entity, _EF81形(x) -> _電気機関車(x).
Hint Resolve ax_isa_EF81形_電気機関車.
Axiom ax_isa_列車名_抽象物 : forall x: Entity, _列車名(x) -> _抽象物(x).
Hint Resolve ax_isa_列車名_抽象物.
Axiom ax_isa_北斗星_列車名 : forall x: Entity, _北斗星(x) -> _列車名(x).
Hint Resolve ax_isa_北斗星_列車名.
Axiom ax_isa_25系_客車 : forall x: Entity, _25系(x) -> _客車(x).
Hint Resolve ax_isa_25系_客車.
Axiom ax_isa_サxmdashxビス_抽象物 : forall x: Entity, _サxmdashxビス(x) -> _抽象物(x).
Hint Resolve ax_isa_サxmdashxビス_抽象物.
Axiom ax_isa_食事提供_サxmdashxビス : forall x: Entity, _食事提供(x) -> _サxmdashxビス(x).
Hint Resolve ax_isa_食事提供_サxmdashxビス.
Axiom ax_isa_作業_抽象物 : forall x: Entity, _作業(x) -> _抽象物(x).
Hint Resolve ax_isa_作業_抽象物.
Axiom ax_isa_除雪作業_作業 : forall x: Entity, _除雪作業(x) -> _作業(x).
Hint Resolve ax_isa_除雪作業_作業.
Axiom ax_isa_運行サxmdashxビス_サxmdashxビス : forall x: Entity, _運行サxmdashxビス(x) -> _サxmdashxビス(x).
Hint Resolve ax_isa_運行サxmdashxビス_サxmdashxビス.
Axiom ax_isa_電車_列車 : forall x: Entity, _電車(x) -> _列車(x).
Hint Resolve ax_isa_電車_列車.
Axiom ax_isa_気動車_列車 : forall x: Entity, _気動車(x) -> _列車(x).
Hint Resolve ax_isa_気動車_列車.
Axiom ax_isa_車輪_部品 : forall x: Entity, _車輪(x) -> _部品(x).
Hint Resolve ax_isa_車輪_部品.
Axiom ax_po_機関車_動力 : forall x : Entity, _機関車(x) -> (exists y : Entity, _動力(y) /\ part_of x y).
Hint Resolve ax_po_機関車_動力.
Axiom ax_po_動力_機関車 : forall x : Entity, _機関車(x) -> (exists y : Entity, _動力(y) /\ part_of y x).
Hint Resolve ax_po_動力_機関車.
Axiom ax_po_機関車_運転台 : forall x : Entity, _機関車(x) -> (exists y : Entity, _運転台(y) /\ part_of x y).
Hint Resolve ax_po_機関車_運転台.
Axiom ax_po_運転台_機関車 : forall x : Entity, _機関車(x) -> (exists y : Entity, _運転台(y) /\ part_of y x).
Hint Resolve ax_po_運転台_機関車.
Axiom ax_po_運転台_運転士 : forall x : Entity, _運転台(x) -> (exists y : Entity, _運転士(y) /\ part_of x y).
Hint Resolve ax_po_運転台_運転士.
Axiom ax_po_運転士_運転台 : forall x : Entity, _運転台(x) -> (exists y : Entity, _運転士(y) /\ part_of y x).
Hint Resolve ax_po_運転士_運転台.
Axiom ax_po_列車_台車 : forall x : Entity, _列車(x) -> (exists y : Entity, _台車(y) /\ part_of x y).
Hint Resolve ax_po_列車_台車.
Axiom ax_po_台車_列車 : forall x : Entity, _列車(x) -> (exists y : Entity, _台車(y) /\ part_of y x).
Hint Resolve ax_po_台車_列車.
Axiom ax_ao_列車_重さ : forall x : Entity, _列車(x) -> (exists y : float, _重さ(x) = y).
Hint Resolve ax_ao_列車_重さ.
Axiom ax_ao_列車_長さ : forall x : Entity, _列車(x) -> (exists y : float, _長さ(x) = y).
Hint Resolve ax_ao_列車_長さ.
Axiom ax_po_ラッセル車_除雪装置 : forall x : Entity, _ラッセル車(x) -> (exists y : Entity, _除雪装置(y) /\ part_of x y).
Hint Resolve ax_po_ラッセル車_除雪装置.
Axiom ax_po_除雪装置_ラッセル車 : forall x : Entity, _ラッセル車(x) -> (exists y : Entity, _除雪装置(y) /\ part_of y x).
Hint Resolve ax_po_除雪装置_ラッセル車.
Axiom ax_po_ラッセル車_作業内容 : forall x : Entity, _ラッセル車(x) -> (exists y : Entity, _作業内容(y) /\ part_of x y).
Hint Resolve ax_po_ラッセル車_作業内容.
Axiom ax_po_作業内容_ラッセル車 : forall x : Entity, _ラッセル車(x) -> (exists y : Entity, _作業内容(y) /\ part_of y x).
Hint Resolve ax_po_作業内容_ラッセル車.
Axiom ax_po_ラッセル車_サxmdashxビス内容 : forall x : Entity, _ラッセル車(x) -> (exists y : Entity, _サxmdashxビス内容(y) /\ part_of x y).
Hint Resolve ax_po_ラッセル車_サxmdashxビス内容.
Axiom ax_po_サxmdashxビス内容_ラッセル車 : forall x : Entity, _ラッセル車(x) -> (exists y : Entity, _サxmdashxビス内容(y) /\ part_of y x).
Hint Resolve ax_po_サxmdashxビス内容_ラッセル車.
Axiom ax_po_気動車_動力 : forall x : Entity, _気動車(x) -> (exists y : Entity, _動力(y) /\ part_of x y).
Hint Resolve ax_po_気動車_動力.
Axiom ax_po_動力_気動車 : forall x : Entity, _気動車(x) -> (exists y : Entity, _動力(y) /\ part_of y x).
Hint Resolve ax_po_動力_気動車.
Axiom ax_po_電気機関車_動力 : forall x : Entity, _電気機関車(x) -> (exists y : Entity, _動力(y) /\ part_of x y).
Hint Resolve ax_po_電気機関車_動力.
Axiom ax_po_動力_電気機関車 : forall x : Entity, _電気機関車(x) -> (exists y : Entity, _動力(y) /\ part_of y x).
Hint Resolve ax_po_動力_電気機関車.
Axiom ax_po_ディxmdashxゼル機関車_動力 : forall x : Entity, _ディxmdashxゼル機関車(x) -> (exists y : Entity, _動力(y) /\ part_of x y).
Hint Resolve ax_po_ディxmdashxゼル機関車_動力.
Axiom ax_po_動力_ディxmdashxゼル機関車 : forall x : Entity, _ディxmdashxゼル機関車(x) -> (exists y : Entity, _動力(y) /\ part_of y x).
Hint Resolve ax_po_動力_ディxmdashxゼル機関車.
Axiom ax_po_先頭制御車_動力 : forall x : Entity, _先頭制御車(x) -> (exists y : Entity, _動力(y) /\ part_of x y).
Hint Resolve ax_po_先頭制御車_動力.
Axiom ax_po_動力_先頭制御車 : forall x : Entity, _先頭制御車(x) -> (exists y : Entity, _動力(y) /\ part_of y x).
Hint Resolve ax_po_動力_先頭制御車.
Axiom ax_po_台車_車輪 : forall x : Entity, _台車(x) -> (exists y : Entity, _車輪(y) /\ part_of x y).
Hint Resolve ax_po_台車_車輪.
Axiom ax_po_車輪_台車 : forall x : Entity, _台車(x) -> (exists y : Entity, _車輪(y) /\ part_of y x).
Hint Resolve ax_po_車輪_台車.
Axiom ax_po_北斗星_牽引車 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _牽引車(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_牽引車.
Axiom ax_po_牽引車_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _牽引車(y) /\ part_of y x).
Hint Resolve ax_po_牽引車_北斗星.
Axiom ax_po_北斗星_1sim5号車lpar禁煙開放B寝台rpar : forall x : Entity, _北斗星(x) -> (exists y : Entity, _1sim5号車lpar禁煙開放B寝台rpar(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_1sim5号車lpar禁煙開放B寝台rpar.
Axiom ax_po_1sim5号車lpar禁煙開放B寝台rpar_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _1sim5号車lpar禁煙開放B寝台rpar(y) /\ part_of y x).
Hint Resolve ax_po_1sim5号車lpar禁煙開放B寝台rpar_北斗星.
Axiom ax_po_北斗星_6号車lpar喫煙B寝台plusロビxmdashxrpar : forall x : Entity, _北斗星(x) -> (exists y : Entity, _6号車lpar喫煙B寝台plusロビxmdashxrpar(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_6号車lpar喫煙B寝台plusロビxmdashxrpar.
Axiom ax_po_6号車lpar喫煙B寝台plusロビxmdashxrpar_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _6号車lpar喫煙B寝台plusロビxmdashxrpar(y) /\ part_of y x).
Hint Resolve ax_po_6号車lpar喫煙B寝台plusロビxmdashxrpar_北斗星.
Axiom ax_po_北斗星_7号車lpar食堂車rpar : forall x : Entity, _北斗星(x) -> (exists y : Entity, _7号車lpar食堂車rpar(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_7号車lpar食堂車rpar.
Axiom ax_po_7号車lpar食堂車rpar_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _7号車lpar食堂車rpar(y) /\ part_of y x).
Hint Resolve ax_po_7号車lpar食堂車rpar_北斗星.
Axiom ax_po_7号車lpar食堂車rpar_サxmdashxビス内容 : forall x : Entity, _7号車lpar食堂車rpar(x) -> (exists y : Entity, _サxmdashxビス内容(y) /\ part_of x y).
Hint Resolve ax_po_7号車lpar食堂車rpar_サxmdashxビス内容.
Axiom ax_po_サxmdashxビス内容_7号車lpar食堂車rpar : forall x : Entity, _7号車lpar食堂車rpar(x) -> (exists y : Entity, _サxmdashxビス内容(y) /\ part_of y x).
Hint Resolve ax_po_サxmdashxビス内容_7号車lpar食堂車rpar.
Axiom ax_po_北斗星_8号車lpar喫煙A寝台rpar : forall x : Entity, _北斗星(x) -> (exists y : Entity, _8号車lpar喫煙A寝台rpar(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_8号車lpar喫煙A寝台rpar.
Axiom ax_po_8号車lpar喫煙A寝台rpar_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _8号車lpar喫煙A寝台rpar(y) /\ part_of y x).
Hint Resolve ax_po_8号車lpar喫煙A寝台rpar_北斗星.
Axiom ax_po_北斗星_9middot10号車lpar喫煙A寝台plusB寝台rpar : forall x : Entity, _北斗星(x) -> (exists y : Entity, _9middot10号車lpar喫煙A寝台plusB寝台rpar(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_9middot10号車lpar喫煙A寝台plusB寝台rpar.
Axiom ax_po_9middot10号車lpar喫煙A寝台plusB寝台rpar_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _9middot10号車lpar喫煙A寝台plusB寝台rpar(y) /\ part_of y x).
Hint Resolve ax_po_9middot10号車lpar喫煙A寝台plusB寝台rpar_北斗星.
Axiom ax_po_北斗星_11号車lpar喫煙開放B寝台rpar : forall x : Entity, _北斗星(x) -> (exists y : Entity, _11号車lpar喫煙開放B寝台rpar(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_11号車lpar喫煙開放B寝台rpar.
Axiom ax_po_11号車lpar喫煙開放B寝台rpar_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _11号車lpar喫煙開放B寝台rpar(y) /\ part_of y x).
Hint Resolve ax_po_11号車lpar喫煙開放B寝台rpar_北斗星.
Axiom ax_po_北斗星_乗客 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _乗客(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_乗客.
Axiom ax_po_乗客_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _乗客(y) /\ part_of y x).
Hint Resolve ax_po_乗客_北斗星.
Axiom ax_po_北斗星_車掌 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _車掌(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_車掌.
Axiom ax_po_車掌_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _車掌(y) /\ part_of y x).
Hint Resolve ax_po_車掌_北斗星.
Axiom ax_po_北斗星_サxmdashxビス内容 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _サxmdashxビス内容(y) /\ part_of x y).
Hint Resolve ax_po_北斗星_サxmdashxビス内容.
Axiom ax_po_サxmdashxビス内容_北斗星 : forall x : Entity, _北斗星(x) -> (exists y : Entity, _サxmdashxビス内容(y) /\ part_of y x).
Hint Resolve ax_po_サxmdashxビス内容_北斗星.
Variable _故障 : Entity -> Prop.
Axiom partial_故障 : forall x y : Entity, _故障(x) -> part_of y x -> _故障(y).
Hint Resolve partial_故障.
Variable _正常 : Entity -> Prop.
Axiom total_正常 : forall x y : Entity, _正常(x) -> part_of y x -> _正常(y).
Hint Resolve total_正常.


(*Theorem test_is_a : (forall x : Entity, _25系(x) -> _列車(x)). 
Proof.
auto.
Qed.

Print test_is_a.

Variable a : Entity.
Axiom part_of_transitivity : (forall x y z : Entity, part_of x y -> part_of y z -> part_of x z).
Hint Resolve part_of_transitivity.

Ltac dst_sigma :=
  match goal with
    | [ x : {y : _ & _ y} |- _ ] => destruct x
  end.

Ltac total_ax :=
  match goal with
    | [ _ : _正常 a, _ : part_of _ a  |- _正常 _ ]
     => apply total_正常 with (x:=a) (* xは公理のx *)
  end.

Ltac ontology_tac := firstorder; try (dst_sigma); total_ax; firstorder.

Theorem test_part_of :
 _列車 a /\ _正常 a ->
 (forall x: Entity, _車輪 x -> part_of x a -> _正常 x).
Proof. ontology_tac. Qed.

Theorem test_part_of' :
 forall u : {y: Entity &  _列車 y /\ _正常 y},
 (forall x: Entity, _車輪 x -> part_of x (projT1 u) -> _正常 x).
Proof. firstorder. destruct u. destruct a0. apply total_正常 with (x:=x0).
apply _0. apply H0. Qed.

(*
Proof. intros h1. intros x h2 h3. destruct h1.
  apply total_正常 with (x:=a);auto.
Qed.
*)

(*
(forall x : Entity, _列車(x)) -> (exists y : Entity, _車輪(y)). 


Theorem test_part_of : (forall x : Entity, _列車(x)) -> (exists y : Entity, _車輪(y)). 
Proof.
intuition.
*)
*)

Variable 行う : Entity -> prop.
Theorem monotonicity : (exists x : Entity, ラッセル車 x /\ 運行 x)　-> (exists x : Entity, 列車 x /\ 行う x 除雪作業 /\ 運行 x).