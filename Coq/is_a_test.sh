#!/bin/bash
# 
# Script to prove "is_a" relationships given an axiom
# database, and two words. Please, use it as:
# ./is_a_test.sh axioms.v word1 word2
# Example:
# ./is_a_test.sh axioms.v 25系 列車
# (it should output "yes"), or
# ./is_a_test.sh axioms.v 列車 25系
# (it should output "unknown")

axioms=$1
word1=$2
word2=$3

echo "" > file.txt
echo "Theorem test_is_a : (forall x : Entity, _"$word1"(x) -> _"$word2"(x))." >> file.txt
echo "Proof. auto. Qed. Print test_is_a." >> file.txt

cat $axioms file.txt > tmp.txt
cat $axioms file.txt | \
  coqtop 2> /dev/null | \
  awk '{if($0 ~ /is defined/) {
          print "yes";
       } else if ($0 ~ /incomplete proof/) {
          print "unknown";
       }}'

