import codecs

filename = 'replacement.txt'
finput = codecs.open(filename, 'r', 'utf-8')
replacement_dict = {}
for line in finput:
  key, value = line.split()
  replacement_dict[key] = value

word = 'PAscual'
new_word = []
for char in word:
  if char in replacement_dict:
    new_char = replacement_dict[char]
    new_word.append(new_char)
  else:
    new_word.append(char)
print(''.join(new_word))
