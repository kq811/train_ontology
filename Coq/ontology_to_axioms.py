#!/bin/python
# coding: UTF-8

from __future__ import print_function
from lxml import etree as etree
import codecs
import re, pprint
ontology_filename = 'ex3.xml'
output_filename = 'axioms.v'
finput = open(ontology_filename, 'rb')
foutput = codecs.open(output_filename, 'w', 'utf-8')
finput_string = finput.read()
finput.close()

filename = 'replacement.txt'
finput = codecs.open(filename,'r','utf-8')
replacement_dict = {}
for line in finput:
  key,value = line.split()
  replacement_dict[key] = value
finput.close()

filename = 'predicate.txt'
finput = codecs.open(filename,'r','utf-8')
predicate_dict = {}
for line in finput:
  key,value = line.split()
  predicate_dict[key] = value
finput.close()

parser = etree.XMLParser(encoding = 'utf-8')
ontology = etree.fromstring(finput_string, parser=parser)

# Now, process the ontology:

# Is-a elements are only inside of the W_CONCEPTS (child 1),
# and they are direct children of the W_CONCEPTS node.


def pp(obj):
  pp = pprint.PrettyPrinter(indent=4, width=160)
  str = pp.pformat(obj)
  return re.sub(r"\\u([0-9a-f]{4})", lambda x: unichr(int("0x"+x.group(1),16)),str)


def FindSlotsInConcept(concept):
  for field in concept:
    if field.tag == 'SLOTS':
      return field
  return None


def GetMainObjectFromConcept(concept):
  for field in concept:
    if field.tag == 'LABEL':
      return field.text
  return None

def ReplaceConflictChars(word, rep_dict):
  new_word = []
  for char in word:
    if char in rep_dict:
      new_char = rep_dict[char]
      new_word.append(new_char)
    else:
      new_word.append(char)
  return ''.join(new_word)

isa_elements = ontology[1].findall('.//ISA')
foutput.write(u'Variable Entity : Type.\n')
foutput.write(u'Variable float : Type.\n')
foutput.write(u'Variable part_of : Entity -> Entity -> Prop.\n')
for isa_element in isa_elements:
   concept = isa_element.get('child')
   concept = ReplaceConflictChars(concept, replacement_dict)
   foutput.write(u'Variable _'+ concept + ': Entity -> Prop.\n')

def PrintType(slots):
  concept_list = []
  for isa_element in isa_elements:
    concept = isa_element.get('child')
    concept = ReplaceConflictChars(concept, replacement_dict)
    concept_list.append(concept)  
  for slot in slots:
    role = slot.get('role',None)
    new_role = ReplaceConflictChars(role, replacement_dict)
    class_constraint = slot.get('class_constraint',None)
    if new_role not in concept_list:
      if new_role not in list:
        if class_constraint == 'float':
          foutput.write(u'Variable _' + new_role + ': Entity -> float.\n')
        else:
          foutput.write(u'Variable _' + new_role + ': Entity -> Prop.\n')
      list.append(new_role)
      #print (pp(list))
    other_slots = FindSlotsInConcept(slot)
    if other_slots is None:
      continue
    PrintType(other_slots)
  return


w_concepts = ontology[1]
list = []
for concept in w_concepts:
  slots = FindSlotsInConcept(concept)
  if slots is None:
    continue
  PrintType(slots)

isa_elements = ontology[1].findall('.//ISA')
for isa_element in isa_elements:
  if isa_element.get('parent') == 'Entity':
   continue
  parent = ReplaceConflictChars(isa_element.get('parent'), replacement_dict)
  child = ReplaceConflictChars(isa_element.get('child'), replacement_dict)
  foutput.write(u'Axiom ax_isa_{1}_{0} : forall x: Entity, _{1}(x) -> _{0}(x).\n'\
        .format(parent, child))
  foutput.write(u'Hint Resolve ax_isa_{1}_{0}.\n'.format(parent,child))

# Part-of relations. These relations are mainly found
# in W_CONCEPTS node, but there will also be present
# in R_CONCEPTS node in the future (when Nakamura-san
# adds them).
#def FindSlotsInConcept(concept):
 # for field in concept:
  #  if field.tag == 'SLOTS':
   #   return field
 # return None

#def GetMainObjectFromConcept(concept):
 # for field in concept:
  #  if field.tag == 'LABEL':
   #   return field.text
  #return None

def PrintPartOfAxiomsFromSlots(main_object, slots):
  for slot in slots:
    new_main_object = ReplaceConflictChars(main_object, replacement_dict)
    role = slot.get('role', None)
    new_role = ReplaceConflictChars(role, replacement_dict)
    class_constraint = slot.get('class_constraint', None)
    if class_constraint == 'float': #attribute-of link
      foutput.write(u'Axiom ax_ao_{0}_{2} : forall x : Entity, _{0}(x) -> (exists y : {1}, _{2}(x) = y).\n' .format(new_main_object, class_constraint, new_role) )
      foutput.write(u'Hint Resolve ax_ao_{0}_{1}.\n'.format(new_main_object,new_role)) 
    else:
      foutput.write(u'Axiom ax_po_{0}_{1} : forall x : Entity, _{0}(x) -> (exists y : Entity, _{1}(y) /\ part_of x y).\n'.format(new_main_object, new_role))
      foutput.write(u'Hint Resolve ax_po_{0}_{1}.\n'.format(new_main_object, new_role))
      foutput.write(u'Axiom ax_po_{1}_{0} : forall x : Entity, _{0}(x) -> (exists y : Entity, _{1}(y) /\ part_of y x).\n'.format(new_main_object, new_role))
      foutput.write(u'Hint Resolve ax_po_{1}_{0}.\n'.format(new_main_object, new_role))
    other_slots = FindSlotsInConcept(slot)
    if other_slots is None:
      continue
    PrintPartOfAxiomsFromSlots(role, other_slots)
  return

w_concepts = ontology[1]
for concept in w_concepts:
  slots = FindSlotsInConcept(concept)
  if slots is None:
    continue
  main_object = GetMainObjectFromConcept(concept)
  assert main_object is not None
  PrintPartOfAxiomsFromSlots(main_object, slots)

#Print partial predicate axioms and total predicate axioms

def PrintPredicateAxioms(pre_dict):
  keys = pre_dict.keys()
  for key in keys:
    key = ReplaceConflictChars(key,replacement_dict)
    foutput.write(u'Variable _' + key + ' : Entity -> Prop.\n')
    if pre_dict[key] == 'p':
      foutput.write(u'Axiom partial_{0} : forall x y : Entity, _{0}(x) -> part_of y x -> _{0}(y).\n'.format(key))
      foutput.write(u'Hint Resolve partial_{0}.\n'.format(key))
    if pre_dict[key] == 't':
      foutput.write(u'Axiom total_{0} : forall x y : Entity, _{0}(x) -> part_of x y -> _{0}(y).\n'.format(key))
      foutput.write(u'Hint Resolve total_{0}.\n'.format(key))

PrintPredicateAxioms(predicate_dict)
  
foutput.close() 
